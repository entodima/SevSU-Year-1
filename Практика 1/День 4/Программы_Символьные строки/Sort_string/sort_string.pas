program sort_string;
  type
    tarr1 = array [0..99] of string;
  var
    names: tarr1;
    n : integer;
    
   procedure sort_direct_insertion(var A: tarr1; n: integer);
    var
      i, j: integer;
      x:string;
    begin
      for i:=1 to n-1 do
        begin
          x:=A[i];
          j:=i-1;
          while j>=0 do
            begin
              if x<A[j]
                then A[j+1]:=A[j]
                else break;
              j:=j-1; 
            end;
          A[j+1]:=x;
        end
    end; 
    
    
    procedure inputarr(var X:tarr1; var k:integer);
       var i:integer;
           f:text;
           file_name:string;
       begin
         writeln ('������� ��� ����� �����:');
         readln(file_name);
         assign(f, file_name);
         reset (f);
         k:=0; {������� �����}
         while not eof(f) do
           begin
             readln(f,X[k]);
             k:=k+1;
           end;
         close (f);  
       end;
       
    procedure putarr(var X:tarr1; k:integer);
       var i:integer;
           f:text;
           file_name:string;
       begin
         writeln ('������� ��� ����� ������:');
         readln(file_name);
         assign(f, file_name);
         rewrite(f);
         for i:=0 to k-1 do writeln(f,X[i]);
         close (f); 
       end;

    begin
      inputarr(names,n);
      sort_direct_insertion(names,n);
      putarr(names,n);
    end.