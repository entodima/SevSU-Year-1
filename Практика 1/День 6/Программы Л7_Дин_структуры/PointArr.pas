Program DinArrStud;
  Uses Crt; {���������� ������ ���������� ���������� 
           � ��������}
  Const MAX_NUM = 4; {����������� �� ����� ��������� � ������}
        
  Type
     Date = record
                day: integer; {����}
                month: integer; {�����}
                year: integer {���}
            end;
     Student = record
                 id:integer; {����� �������� ������}
                 name: string[15];{������� �.�.}
                 birthday: Date; {���� ��������}
                 ball:real {���� �� ��������� ������}
               end;
     StudPoint = ^Student;  {������ �� ������ � ��������}        
     Group = array [1..MAX_NUM] of StudPoint; {������ ������}
   Var
      i,id,
      n:integer;{�������� ����� ��������� � ������}
      stud:Student; {�������}
      gr:Group;{������������ ������}
      Number: integer;{����� ������ ����}
     
   function addStud (var gr:Group; var n:integer; stud:Student):integer;
      {���������� �������� stud � ������ gr}
       var i:integer;
       begin
        {����� � ������ ��� ���� ����������� ��������� ���������� ���������?}
        if n=MAX_NUM then
                       begin
                          addstud:=2;{�������� ����� ������}
                          exit {��������� ����� �� ������������}
                       end;   
        {����� ������� � ����� id ��� ���� � ������?}
         for i:=1 to n do
           if gr[i]^.id = stud.id
              then
                begin
                  addStud:=1;{������� ��� ����}
                  exit  {��������� ����� �� ������������}
                end; 
         {��� � ������� - ���������!}       
         n:=n+1; 
         new(gr[n]); {������� ������ �������� � ������������ ������}
         gr[n]^:=stud;{��������� ���� �������� -
                       �������� ���������� ������� �������}
         addStud:=0 {������� ������� ��������}
       end;
       
     function delStud (var gr:Group; var n:integer; id:integer):boolean;
       {���������� �������� �� ������ gr �� id}
       var i,j :integer;
       begin
         if n > 0 
           then
             begin {���� �������� �� ������� stud}
               i:=1;
               repeat 
                 if gr[i]^.id = id then break;
                 i:=i+1
               until i>n;
               if i<=n {������� �������}
                 then
                   begin
                     dispose(gr[i]); {������� ��������, ����������� ������}
                     {�������� ���������, �������� �������������� �����,
                     �� ����� ���� �������� � ����������� �� ���������:}
                     for j:=i to n-1 do gr[j]:=gr[j+1];
                     n:=n-1; {��������� ����� ���������}
                     delStud:=true; {�������� ��������}
                   end
                 else delStud:=false {�������}
             end    
           else delstud:=false; {�������}
        end;
     function avgBall (var gr:group; n: integer):real;
        {���������� �������� ����� �� ������}
        var i:integer; res:real;
        begin
          for i:=1 to n do res:=res+gr[i]^.ball;
          avgBall:=res/n
        end;
        
     procedure sortBallDesc (var gr: group; n:integer);
       {���������� ��������� � ������ �� �������� ����� -
        ����� �������� 2}
       var
         j: integer; b: StudPoint; flag:boolean;
       begin
         flag:=false;
         while not flag do {���� ������ �� ������������}
           begin
             flag:=true; {��� �� - ��������}
             for j:=1 to n-1 do {�������� ������� � ������}
               if gr[j]^.ball<gr[j+1]^.ball
                 then
                    begin
                      {�������� � �����������:}
                      b:=gr[j];
                      gr[j]:=gr[j+1];
                      gr[j+1]:=b; {��������� ������������}
                      flag:=false;
                      break;{��������� ����� �� for}
                    end;
           end
    end;  
     
     procedure inputStud (var stud:Student);
       {���� ������ � ����� ��������}
       begin
         writeln('���� ������ � ��������:');
         write ('����� �������� ������: '); readln(stud.id);
         write ('������� � ��������: '); readln(stud.name);
         writeln ('���� ��������: ');
         write ('����: '); readln(stud.birthday.day);
         write ('�����: '); readln(stud.birthday.month);
         write ('���: '); readln(stud.birthday.year);
         write ('����: '); readln(stud.ball);
       end;
       
       procedure putGroup (var gr:Group; n :integer);
         {����� ������ ������}
         var i:integer;
         begin
            writeln ('������ ������ - ',n,' ���������:');
            for i:=1 to n do
              writeln (gr[i]^.id:10, ' ',gr[i]^.name:15, '    ',
                       gr[i]^.birthday.day:2, '.',gr[i]^.birthday.month:2,'.',
                       gr[i]^.birthday.year:4, '    ', gr[i]^.ball:4:2);
         end;              
           
    begin
      n:=0; {� ������ ���� ��� ���������}
      While True Do
        Begin
          ClrScr; { ������� ������ }
          Writeln('1-���������� �������� � ������');
          Writeln('2-���������� �������� �� ������');
          Writeln('3-����� ������ ������');
          Writeln('4-���������� �������� ����� �� ������');
          Writeln('5-���������� ��������� �� �������� �����');
          Writeln('6-�����');
          Writeln('-------------------------------');
          Writeln('������� ����� ������ ����');
          Readln(Number);
          Case Number Of {����� ����������� ��������� �� ������}
                1: begin
                      inputStud (stud);
                      case addStud (gr,n,stud)of
                        0:writeln ('������� ������� ��������.');
                        1:writeln ('�� ������� �������� ��������',
                           ' - ��������� ����������� ������������ id.');
                        2:writeln ('�� ������� �������� ��������',
                                   ' - ����� ��������.')
                      end                    
                   end;       
                2: begin 
                       write('������� ����� �������� ������ ��������: ');
                       readln (id);
                       if delStud (gr,n,id) 
                           then writeln ('������� ������� ��������.')                                             
                           else writeln ('�� ������� ��������� ��������',
                                         ' - ������� �� ������.')  
                   end;        
                3: putGroup (gr,n);
                4: Writeln ('������� ���� �� ������: ', avgBall(gr,n):5:2);
                5: begin
                     sortBallDesc(gr,n);
                     writeln('��������� ���������� ��������� �� �������� �����');
                   end;  
                6:Exit { ���������� ��������� ������}
             End;
             writeln('������� <Enter>');
             readln; {������������� �������� �� ����� �������}
        End
    end.
          
    
