program Pract5;

type
  train = record
    time: string[5];          //����� �����������
    destination: string[30];  //����� ����������
    numbOfTrain: string;      //����� ������
  end;

var
  T1, T2: train;
  sity: string;

{1}
procedure input(var Tn: train; n: byte);
var
  F: text;
  i: integer;
begin
  write('������� ����� ����������� ������ �', n, ': ');
  readln(Tn.time);
  
  write('������� ����� ���������� ������ �', n, ': ');
  readln(Tn.destination);
  
  write('������� ����� ������ �', n, ': ');
  readln(Tn.numbOfTrain);
end;

procedure sortOutput(T1, T2: train);
var
  a: text;
  TXp: train;
begin
  assign(a, 'Z:\Desktop\train.txt');
  rewrite(a);
  
  if T1.time < T2.time then 
  begin
    writeln(a, T1.time, '  ', T1.destination, '  ', T1.numbOfTrain);
    writeln(a, T2.time, '  ', T2.destination, '  ', T2.numbOfTrain);
  end
    else
  begin
    writeln(a, T2.time, '  ', T2.destination, '  ', T2.numbOfTrain);
    writeln(a, T1.time, '  ', T1.destination, '  ', T1.numbOfTrain);
  end;
  close(a);
end;

{2}
procedure inputTXT(T1, T2: train);
var
  a: text;
begin
  assign(a, 'Z:\Desktop\train.txt');
  reset(a);
  readln(a, T1.time, T1.destination, T1.numbOfTrain);
  readln(a, T2.time, T2.destination, T2.numbOfTrain);
  Close(a);
end;

{3,4}
procedure info(sity: string; T1, T2: train);
begin
  if sity = T1.destination
  then
  begin
    Writeln('��������� ������:');
    writeln(T1.time, '  ', T1.destination, '  ', T1.numbOfTrain);
  end
  else
  if sity = T2.destination
    then
  begin
    Writeln('��������� ������:');
    writeln(T2.time, '  ', T2.destination, '  ', T2.numbOfTrain);
  end
  else
    writeln('������ ������ ���');
end;

begin
  {1}
  input(T1, 1);
  input(T2, 2);
  sortOutput(T1, T2);
  
  {2 ������ ������ �� �����}
  inputTXT(T1, T2);
  
  {3,4}
  write('������� �������� ������ ����������: ');
  readln(sity);
  info(sity, T1, T2)
end.