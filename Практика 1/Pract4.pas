program pr4;

const
  max_num_words = 127;
  delim = [',', ' ', '?'];
  numbs = ['0'..'9'];

type
  mass = array[1..max_num_words] of string;

var
  source: string;
  words,numWords: mass;
  n, m, i: byte;
  max,min:byte;


procedure splitIntoWords(str: String; var words: mass; var n: byte; limit: byte);
var
  i: byte;
begin
  i := 1;
  {�������� ��������� � ������ � � �����}

  while((Str[i] = ',') or (Str[i] = ' ') or (Str[i] = '?')) do inc(i);
  delete(str, 1, i - 1);
  i := length(str);
  while((Str[i] = ',') or (Str[i] = ' ') or (Str[i] = '?')) do dec(i);
  delete(str, i + 1, length(str) - i);
  words[i] := str;
  
  {����� ����}
  n := 1;
  i:=1;
  for i:=1 to length(str) do
      if str[i] in delim  then begin
        if (i<>1) and not(str[i-1] in delim) then
          inc(n);
        end
      else 
        words[n] += str[i]; 
end;

procedure numbInWords(var words:mass; n:byte; var numWords:mass; var m:byte);
var i,k,z:byte;
begin
  m:=0;
  for z:=1 to n do
  begin
    for i:=1 to length(words[z]) do
      begin
        if words[z][i] in numbs then
        begin
          m:=m+1;
          numWords[m]:=words[z];
          break;
        end;
      end;
  end;
end;

function strFromMass(numWords:mass; max,min,m:byte):string;
var i,n:byte;
    str:string;

begin
  for i:=1 to m do
  begin
    for n:=1 to length(numWords[i]) do
      begin
        if (length(numWords[i])<=max) and (length(numWords[i])>=min)
        then str:=str+' '+numwords[i];
        strFromMass:=str;
        break;
      end;
   end;
end;

begin
  write('������� �������� ������: ');
  readln(source);
  splitIntoWords(source, words, n, max_num_words);
  
  numbInWords(words,n,numwords,m);
  writeln('�������� �� 2 �������:');
  for i:=1 to m do writeln(numWords[i]);
  
  write('������� ����������� ����� ����: ');
  readln(min);
  write('������� ������������ ����� ����: ');
  readln(max);
  
  Writeln('������, ���������� ����� � ������� � ������� ������ �� ',min,' �� ', max);
  Writeln(strFromMass(numWords,max,min,m));
  
end.