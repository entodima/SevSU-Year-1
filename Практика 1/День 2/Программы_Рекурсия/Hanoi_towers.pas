Program Hanoi_towers;
  const maxDisks = 64;
  type
    st = (left, middle, right); //��������
    natur = 1..maxDisks; //���������� ������
 
  var
    m: natur; {m � ����� ������ � ���������}
 
  procedure move_many(n: natur; s1, sw, sk: st);
    {����������� n ������ � s1(��������� ��������)
     �� sk (�������� ��������) � �������
     sw (���������������� �������)}
      procedure move_one;
        {����������� ������ ����� � s1 �� sk}
         procedure print(s: st);
            {����� �������� ������� s}
             begin
                case s of
                    left: write(' ���. ');
                    middle: write(' �����. ');
                    right: write(' ����. ')
                end;
             end;{print}
             
         begin {move_one}
            write(' ����� ���� � ');
            print(s1);
            write(' ������ �� ');
            print(sk);
            writeln
        end;{move_one}
 
    begin {move_many}
        if n = 1 then {���� ���� ���� - ����������� � s1 �� sk:}
            move_one
        else {�����:}
          begin
            move_many(n - 1, s1, sk, sw);{����������� n-1 ������ 
            � ���������� ������� �� ��������������� � ������� 
            ���������}
            move_one; {����������� ���������� ���� � ����������
            ������� �� ��������}
            move_many(n-1, sw, s1, sk){����������� n-1 ������ 
            � ���������������� ������� �� �������� � ������� 
            ����������}
          end
    end;{move_many}
 
begin
    writeln ('������� ����� ������');
    read(m); {����� ������}
    writeln('��� ', m:3, ' ������ ������� ���������� ',
    '��������� ��������:');
    move_many(m, left, right, middle);
readln
end.