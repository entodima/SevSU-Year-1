program DemoSet; {������ �� �� ������� ��}
  uses Crt; { �������� ����� ������, 
              ����������� ��������� ClrScr}
  var 
    S: set of 'A' .. 'z'; { �������� ��������� }
    number: Integer;
  procedure Konstr; {��������� �������� ���������}
    var C: char;
    begin
      S:=[];
      Writeln('������� ����� �� A �� z');
      Readln(C);
      While C<>'*' do {���� �� ������ ������ '*'}
         begin
            S:=S+[C]; {���������� ��������}
            Readln(C)
         end
    end;
    
  procedure Smin; {��������� ������ ������ � �����������
                    �����, �������� �� ��������� S}
    var C: char;
    begin
       C:='A';
       while not (C in S) do C:=succ(C);
       writeln('������ � �������. �����: ', C);
       Writeln('������� ������� Enter');
       readln;
    end;
    
  procedure Rasp; { ��������� ������ ��������� ��������� S 
                   � ���������� �������}
    var C:char;
    begin
       writeln('������, �������� � ���������:');
       for C:='A' to 'z' do
         if C in S then writeln(C);
       writeln('������� ������� Enter');
       readln;
    end;
{===========�������� ���������=====================}
  begin
   {����������� ����, �������������� ����� �� �����
    ������� ����}
     while true do
        begin
          ClrScr; { ������� ������ }
          Writeln('1-������������ ���������');
          Writeln('2-����� �������� � ���. �����');
          Writeln('3-������ ���������');
          Writeln('4-�����');
          Writeln('-------------------------------');
          Writeln('������� ����� ������ ����');
          Readln(number);
          case number Of{ ����� ����������� ��������� �� ������}
             1:Konstr;
             2:Smin;
             3:Rasp;
             4:Exit { ���������� ��������� ������}
          end
       end
  end.