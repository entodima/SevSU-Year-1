program abc;
  type group=set of 1..255; {������ ���������}
  var
     f:text;
     gr1,gr2:group;
     num, s: byte;
  function length(var gr:group):byte;
     var c,i:byte;
     begin
       for i:=1 to 255 do
          if i in gr then c:=c+1;
       length:=c
     end;  
  begin
  {-----------------------1)����� � ������------------------------}
     gr1:=[]; gr2:=[];
     assign(f,'data.txt');
     reset (f);
     while not eof(f) do
        begin
          readln(f,num);
          writeln ('� ����� ������ ������� �������� ',num,':');
          writeln ('1 - ��������, 2 - ������, 3 - � ��� ');
          readln (s);
          case s of
            1: gr1:=gr1+[num];
            2: gr2:=gr2+[num];
            3: begin gr1:=gr1+[num]; gr2:=gr2+[num]; end
          end  
        end;
     close(f);
  {---------------------2)����� ������� ������--------------------------}
     assign(f,'result.txt');
     rewrite(f);
     writeln(f,'������ ���������: ',gr1);
     writeln(f,'������ �������: ',gr2);
  {---------------------3)����� ����������� ������------------------}   
     writeln (f,'����� ��������� � ������ ���������: ', length(gr1));
     writeln (f,'����� ��������� � ������ �������: ', length(gr2));
  {--------------------4)������ ������� ������-------------------------}
     if gr1>=gr2 
      then
       writeln(f,'������ ������ ��������� �������� ������ ������ �������')
      else if gr2>=gr1
             then
               writeln(f,'������ ������ ������� �������� ������ ������ ���������')
             else
               writeln(f,'��������� � ������ ������� �� �����������'); 
  {------------------5)6)7)��������� ���������--------------------------}              
     writeln (f,'��������, ������������ � �������� � ����������:',gr1*gr2);
     writeln (f,'��������, ������������ ������ ����������:',gr1-gr2);
     writeln (f,'��������, ������������ ������ ��������:',gr2-gr1);
  {------------------------8) ���������� �� ������----------------------}
     writeln ('��������: ',gr1);
     writeln ('������: ',gr2);
     writeln ('������� ����� ������������ ��������');
     readln (num);
     exclude(gr1,num); exclude(gr2,num);
     writeln (f,'�������� ����� ���������� ',num,': ',gr1);
     writeln (f,'������ ����� ���������� ',num,': ',gr2);
     close(f)
  end.   