program abc;
  const
     let_set1=['a','e','f'];
     let_set2=['a','n'];
  type
     Letters = set of char;
  var
     a1, a2 :Letters; 
     b1, b2 :set of 1..3;
  begin
     a1:=let_set1;
     a2:=let_set2;
     b1:=[1,3]; b2:=[1,2,3];
     if (('e' in a1) or ('e' in a2)) and
        ((2 in b1) or (2 in b2))
        then writeln ('*****');
  end.   