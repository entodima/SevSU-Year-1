program abc;
  type
     tarr1=array [1..100] of real;
     tmatr1=array ['a' .. 'm', 1..10] of integer;
  var
     A:tarr1; 
     M:tmatr1;
  begin
     A[5]:=2.5;
     M['b',4]:=-20;
     writeln (A[5]:5:2, M['b',4]:5);
  end.   
