program lr2;

var
  z, k, a: real;

begin
  write('z=');
  readln(z);  
  a := arctan(z);  
  if a >= pi / 4 
    then    k := -0.706E-4  
  else if a < -pi / 4
    then k := sin(abs(a))
  else k := sin(a / 2);
  
  writeln(k)
end.